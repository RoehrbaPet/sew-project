﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class SortedList<K, T> : AList<K, T> where K : IComparable<K>
    {
        public SortedList()
            : base()
        { }

        public SortedList(ListNode<K, T>[] arr)
            : base(arr)
        { }
        protected override void InsertFromArray(ListNode<K, T>[] arr)
        {
            foreach (ListNode<K, T> node in arr)
            {
                InsertSorted(node);
            }
        }
        public void InsertSorted(ListNode<K, T> node)
        {
            if (Head == null)
            {
                Head = node;
                Tail = node;
                return;
            }
            if (node.Key.CompareTo(Head.Key) <= 0)
            {
                InsertHead(node);
                return;
            }

            ListNode<K, T> cur = Head;
            ListNode<K, T> tmp = cur.Next;

            while (node.Key.CompareTo(cur.Key) > 0)
            {
                if (tmp == null)
                {
                    InsertTail(node);
                    return;
                }
                else
                {
                    cur = tmp;
                    tmp = cur.Next;
                }
            }
            tmp = cur;
            cur = tmp.Prev;
            
            cur.Next = node;
            node.Prev = cur;
            node.Next = tmp;
            tmp.Prev = node;
        }
        public void InsertSorted(K key, T value)
        {
            InsertSorted(new ListNode<K, T>(key,value));
        }
    }
}
