﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class BTree<K, T> : IEnumerable<TreeNode<K,T>> where K : IComparable<K>
    {
        public TreeNode<K, T> Root { get; set; } = null;
        private ITraverse<K, T> traverse = new Inorder<K, T>();

        public BTree() { }

        public BTree(TreeNode<K, T>[] arr)
        {
            foreach (TreeNode<K, T> cur in arr)
                Insert(cur);
        }

        public IEnumerator<TreeNode<K,T>> GetEnumerator()
        {
            return new BTreeIterator<K, T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new BTreeIterator<K, T>(this);
        }

        public TreeNode<K, T> Find(K key)
        {
            TreeNode<K, T> cur = Root;

            while (cur != null && key.CompareTo(cur.Key) != 0)
            {
                if (key.CompareTo(cur.Key) < 0)
                    cur = cur.Left;
                else
                    cur = cur.Right;
            }

            return cur;
        }

        public bool Contains(TreeNode<K, T> node)
        {
            return Contains(node.Key);
        }
        public bool Contains(K key)
        {
            return Find(key) != null;
        }

        public void Insert(TreeNode<K, T> node)
        {
            if (Root == null)
            {
                Root = node;
                return;
            }

            TreeNode<K, T> cur = Root;

            while (true)
            {
                if (node.Key.CompareTo(cur.Key) <= 0)
                {
                    if (cur.Left == null)
                        break;
                    cur = cur.Left;
                }
                else
                {
                    if (cur.Right == null)
                        break;
                    cur = cur.Right;
                }
            }

            if (node.Key.CompareTo(cur.Key) <= 0)
                cur.Left = node;
            else
                cur.Right = node;
        }
        public void Insert(K key, T value)
        {
            Insert(new TreeNode<K, T>(key, value));
        }

        public void SetTraverse(ITraverse<K, T> traverse)
        {
            this.traverse = traverse;
        }

        public override string ToString()
        {
            return traverse.Traverse(this);
        }
    }
}
