﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public interface ISortBehaviour<K, T> where K : IComparable<K>
    {
        void Sort(LinkedList<K, T> list);
    }
}
