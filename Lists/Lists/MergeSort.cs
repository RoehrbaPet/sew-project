﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class MergeSort<K, T> : ISortBehaviour<K, T> where K : IComparable<K>
    {
        int count;

        public void Sort(LinkedList<K, T> list)
        {
            ListNode<K, T> curr = list.Head;
            while (curr != null)
            {
                count++;
                curr = curr.Next;
            }
            MergeSorter(list, 0, count - 1);
        }

        private void MergeSorter(LinkedList<K, T> list, int left, int right)
        {
            int mid;

            if (left < right)
            {
                mid = (left + right) / 2;
                MergeSorter(list, left, mid);
                MergeSorter(list, mid + 1, right);
                Merge(list, left, right, mid);
            }
        }

        private void Merge(LinkedList<K, T> list, int left, int right, int mid)
        {
            int i, j;
            LinkedList<K, T> newlist = new LinkedList<K, T>();

            for (int n = 0; n < left; n++)
                newlist.InsertLast(Clone(GetNode(n, list)));

            i = left;
            j = mid + 1;
            
            while (i <= mid && j <= right)
            {
                if (GetNode(i, list).Key.CompareTo(GetNode(j, list).Key) < 0)
                    newlist.InsertLast(Clone(GetNode(i++, list)));
                else
                    newlist.InsertLast(Clone(GetNode(j++, list)));
            }

            while (i <= mid)
                newlist.InsertLast(Clone(GetNode(i++, list)));
            while (j <= right)
                newlist.InsertLast(Clone(GetNode(j++, list)));

            for (int n = right + 1; n < count; n++)
                newlist.InsertLast(Clone(GetNode(n, list)));

            list.Override(newlist);
        }

        private ListNode<K,T> GetNode(int index, LinkedList<K, T> list)
        {
            ListNode<K,T> curr = list.Head;
            for (int i = 0; i < index; i++)
            {
                curr = curr.Next;
            }
            return curr;
        }

        private ListNode<K,T> Clone(ListNode<K, T> n)
        {
            return new ListNode<K, T>(n.Key, n.Value);
        }
    }
}
