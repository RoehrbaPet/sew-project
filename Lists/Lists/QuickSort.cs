﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class QuickSort<K, T> : ISortBehaviour<K, T> where K : IComparable<K>
    {
        int count;
        public void Sort(LinkedList<K, T> list)
        {
            for (ListNode<K, T> curr = list.Head; curr != null; curr = curr.Next)
                count++;

            QuickSortLeft(list, 0, count - 1);
        }

        public void QuickSortLeft(LinkedList<K, T> list, int left, int right)
        {
            int i;

            if (left < right)
            {
                i = SplitRight(list, left, right);
                QuickSortLeft(list, left, i - 1);
                QuickSortLeft(list, i + 1, right);
            }
        }

        public int SplitRight(LinkedList<K, T> list, int left, int right)
        {
            int k;
            
            ListNode<K,T> pivot = GetNode(right, list);
            int i = left -1;

            for (k = left; k <= right-1; k++)
            {
                if (pivot.Key.CompareTo(GetNode(k,list).Key) >= 0)
                {
                    i++;
                    list.Swap(GetNode(i, list), GetNode(k, list));
                }
            }
            i++;
            list.Swap(pivot, GetNode(i,list));
            return i;
        }

        public ListNode<K, T> GetNode(int index, LinkedList<K, T> list)
        {
            ListNode<K, T> curr = list.Head;
            for (int i = 0; i < index; i++)
            {
                curr = curr.Next;
            }
            return curr;
        }
    }
}
