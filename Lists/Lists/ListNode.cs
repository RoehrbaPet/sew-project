﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class ListNode<K, T> : Node<K, T> where K : IComparable<K>
    {
        public ListNode<K, T> Prev { get; set; }
        public ListNode<K, T> Next { get; set; }

        public ListNode(K key, T value, ListNode<K, T> prev = null, ListNode<K, T> next = null)
            : base(key, value)
        {
            this.Prev = prev;
            this.Next = next;
        }
    }
}
