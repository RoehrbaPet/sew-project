﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class InsertionSort<K, T> : ISortBehaviour<K, T> where K : IComparable<K>
    {
        public void Sort(LinkedList<K, T> list)
        {
            SortedList<K, T> sortedList = new SortedList<K, T>();

            while (list.Head != null)
                sortedList.InsertSorted(list.Remove(list.Head));

            list.Override(sortedList);
        }
    }
}
