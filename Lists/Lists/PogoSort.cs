﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class PogoSort<K, T> : ISortBehaviour<K, T> where K : IComparable<K>
    {        
        int count;

        public void Sort(LinkedList<K, T> list)
        {
            Random rnd = new Random();

            for (ListNode<K, T> curr = list.Head;  curr != null; curr = curr.Next)
                count++;

            LinkedList<K, T> newlist;
            do
            {
                newlist = new LinkedList<K, T>();

                LinkedList<K, T> listclone = new LinkedList<K, T>();
                for (int i = 0; i < count; i++)
                    listclone.InsertLast(Clone(GetNode(i, list)));
                int countclone = count;

                while (listclone.Head != null)
                    newlist.InsertLast(Clone(listclone.Remove(GetNode(rnd.Next(0, countclone--), listclone))));
            } while (!newlist.IsSorted());

            list.Override(newlist);
        }

        public ListNode<K, T> GetNode(int index, LinkedList<K, T> list)
        {
            ListNode<K, T> curr = list.Head;
            for (int i = 0; i < index; i++)
            {
                curr = curr.Next;
            }
            return curr;
        }

        private ListNode<K, T> Clone(ListNode<K, T> n)
        {
            return new ListNode<K, T>(n.Key, n.Value);
        }
    }
}
