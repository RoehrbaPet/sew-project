﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class SelectionSort<K, T> : ISortBehaviour<K, T> where K : IComparable<K>
    {
        public void Sort(LinkedList<K, T> list)
        {
            LinkedList<K, T> sortedList = new LinkedList<K, T>();

            while (list.Head != null)
                sortedList.InsertLast(list.Remove(GetMinimum(list)));

            list.Override(sortedList);
        }
        

        ListNode<K, T> GetMinimum(LinkedList<K, T> list)
        {
            ListNode<K, T> minimum = list.Head;
            for(ListNode<K, T> curr = list.Head; curr != null; curr = curr.Next)            
                if (curr.Key.CompareTo(minimum.Key) < 0)
                    minimum = curr;
            return minimum;
        }
    }
}
