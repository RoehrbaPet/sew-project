﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class ListIterator<K, T> : IEnumerator<ListNode<K,T>> where K : IComparable<K>
    {
        private AList<K, T> list;

        public ListNode<K, T> Current { get; private set; }
        object IEnumerator.Current { get { return Current; } }

        private bool survivedInitalStep = false;

        public ListIterator(AList<K, T> list)
        {
            this.list = list;
            this.Current = list.Head;
        }

        public void Dispose() //geht das so?
        {

        }

        public bool MoveNext()
        {
            if (!survivedInitalStep)
            {
                survivedInitalStep = true;
                return true;
            }

            if (Current == null)
                return false;

            Current = Current.Next;
            return Current != null;
        }

        public void Reset()
        {
            Current = list.Head;
            survivedInitalStep = false;
        }
    }
}
