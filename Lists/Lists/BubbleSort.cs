﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class BubbleSort<K, T> : ISortBehaviour<K, T> where K : IComparable<K>
    {
        public void Sort(LinkedList<K, T> list)
        {
            if (list.Head == null)
                return;

            bool sorted = false;
            while (!sorted)
            {
                sorted = true;

                for (ListNode<K, T> curr = list.Head; curr.Next != null;)
                {
                    if (curr.Key.CompareTo(curr.Next.Key) > 0)
                    {
                        list.Swap(curr, curr.Next);
                        sorted = false;
                    }
                    else
                        curr = curr.Next;
                }
            }
        }
    }
}
