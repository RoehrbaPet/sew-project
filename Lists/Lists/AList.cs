﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public abstract class AList<K, T> : IEnumerable<ListNode<K,T>> where K : IComparable<K>
    {
        public ListNode<K, T> Head { get; protected set; }
        public ListNode<K, T> Tail { get; protected set; }
        
        public AList()
        {
            Head = null;
            Tail = null;
        }
        public AList(ListNode<K,T>[] arr)
        {
            InsertFromArray(arr);
        }

        protected abstract void InsertFromArray(ListNode<K,T>[] arr);

        public IEnumerator<ListNode<K,T>> GetEnumerator()
        {
            return new ListIterator<K, T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ListIterator<K,T>(this);
        }

        public ListNode<K, T> Find(K key)
        {
            ListNode<K, T> cur = Head;
            while (cur != null)
            {
                if (cur.Key.CompareTo(key) == 0)
                    return cur;
                else
                    cur = cur.Next;
            }
            return null;
        }

        public bool Contains(K key)
        {
            if (Find(key) == null)
                return false;
            else
                return true;
        }
        public bool Contains(ListNode<K, T> node)
        {
            ListNode<K, T> cur = Head;
            while (cur != null)
            {
                if (cur.Equals(node))
                    return true;
                else
                    cur = cur.Next;
            }
            return false;
        }

        public void DeleteAt(K key)
        {
            if (Head == Tail && Head.Key.CompareTo(key) == 0)
            {
                Head = null;
                Tail = null;
            }
            ListNode<K, T> cur = Head;
            while (cur != null)
            {
                if (cur.Key.CompareTo(key) == 0)
                {
                    if (cur == Head)
                    {
                        Head = cur.Next;
                        Head.Prev.Next = null;
                        Head.Prev = null;
                        cur = null;
                    }
                    else if (cur == Tail)
                    {
                        Tail = cur.Prev;
                        Tail.Next.Prev = null;
                        Tail.Next = null;
                        cur = null;
                    }
                    else
                    {
                        ListNode<K, T> tmp = cur.Next;
                        cur.Prev.Next = tmp;
                        tmp.Prev = cur.Prev;
                        cur = null;
                    }
                }
                else
                {
                    cur = cur.Next;
                }
            }
        }
        public void Delete(ListNode<K, T> node)
        {
            DeleteAt(node.Key);
        }

        public ListNode<K,T> Remove(ListNode<K,T> node)
        {
            if (Head == Tail && Head == node)
            {
                Head = null;
                Tail = null;
                return node;
            }
            ListNode<K, T> cur = Head;
            while (cur != null)
            {
                if (cur.Key.CompareTo(node.Key) == 0)
                {
                    if (cur == Head)
                    {
                        Head = cur.Next;
                        cur.Next = null;
                        if (Head != null)
                            Head.Prev = null;
                    }
                    else if (cur == Tail)
                    {
                        Tail = cur.Prev;
                        cur.Prev = null;
                        if (Tail != null)
                            Tail.Next = null;
                    }
                    else
                    {
                        ListNode<K, T> tmp = cur.Next;
                        cur.Prev.Next = tmp;
                        tmp.Prev = cur.Prev;
                    }
                    return cur;
                }
                else
                {
                    cur = cur.Next;
                }
            }
            return null;
        }

        protected void InsertHead(ListNode<K, T> node)
        {
            if (Head == null)
            {
                Head = node;
                Tail = node;
            }
            else if (Head == Tail)
            {
                Head = node;
                Head.Next = Tail;
                Tail.Prev = Head;
            }
            else
            {
                ListNode<K, T> tmp = Head;
                Head = node;
                Head.Next = tmp;
                tmp.Prev = Head;
            }
        }
        protected void InsertTail(ListNode<K, T> node)
        {
            if (Head == null)
            {
                Head = node;
                Tail = node;
            }
            else if (Head == Tail)
            {
                Tail = node;
                Tail.Prev = Head;
                Head.Prev = null;
                Head.Next = Tail;
                Tail.Next = null;
            }
            else
            {
                ListNode<K, T> tmp = Tail;
                Tail = node;
                Tail.Prev = tmp;
                tmp.Next = Tail;
            }
        }

        public bool IsSorted()
        {
            if (Head == null)
                return true;
            ListNode<K, T> cur = Head;
            while (cur != null)
            {
                if (cur.Next == null)
                {
                    return true;
                }
                else if (cur.Key.CompareTo(cur.Next.Key) > 0)
                    return false;
                cur = cur.Next;
            }
            return true;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            ListNode<K, T> cur = Head;
            while (cur != null)
            {
                sb.Append(cur + "|");
                cur = cur.Next;
            }
            return sb.ToString();
        }

        public string ReversePrint()
        {
            StringBuilder sb = new StringBuilder();
            ListNode<K, T> cur = Tail;
            while (cur != null)
            {
                sb.Append(cur + "|");
                cur = cur.Prev;
            }
            return sb.ToString();
        }

        public void Override(AList<K, T> list)
        {
            this.Head = list.Head;
            this.Tail = list.Tail;
        }
    }
}
