﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public abstract class Node<K,T> where K : IComparable<K>
    {
        public K Key { get; set; }
        public T Value { get; set; }

        public Node(K key, T value)
        {
            this.Key = key;
            this.Value = value;
        }

        public override string ToString()
        {
            return Key.ToString() + ": " + Value.ToString();
        }
    }
}
