﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class LinkedList<K, T> : AList<K, T> where K : IComparable<K>
    {
        private ISortBehaviour<K, T> sortBeh;

        public LinkedList()
            : base()
        {  }

        public LinkedList(ListNode<K, T>[] arr)
            : base(arr)
        { }

        protected override void InsertFromArray(ListNode<K, T>[] arr)
        {
            foreach (ListNode<K,T> node in arr)
            {
                InsertLast(node);
            }
        }

        public void SetSortBehaviour(ISortBehaviour<K, T> beh)
        {
            this.sortBeh = beh;
        }
        public void Sort()
        {
            sortBeh.Sort(this);
        }

        public void InsertFirst(ListNode<K, T> node)
        {
            InsertHead(node);
        }
        public void InsertFirst(K key, T value)
        {
            InsertHead(new ListNode<K, T>(key,value));
        }

        public void InsertLast(ListNode<K, T> node)
        {
            InsertTail(node);
        }
        public void InsertLast(K key, T value)
        {
            InsertTail(new ListNode<K, T>(key,value));
        }

        public void Swap(ListNode<K, T> n, ListNode<K, T> m)
        {
            if (n == Head)
                Head = m;
            else if (m == Head)
                Head = n;
            if (n == Tail)
                Tail = m;
            else if (m == Tail)
                Tail = n;

            if (n.Next == m)
            {
                ListNode<K, T> np = n.Prev, mn = m.Next;

                if (np != null)
                    np.Next = m;
                m.Prev = np;
                m.Next = n;
                n.Prev = m;
                if (mn != null)
                    mn.Prev = n;
                n.Next = mn;
            }
            else if (m.Next == n)
            {
                ListNode<K, T> mp = m.Prev, nn = n.Next;
                if (mp != null)
                    mp.Next = n;
                n.Prev = mp;
                m.Prev = n;
                n.Next = m;
                if (nn != null)
                    nn.Prev = m;
                m.Next = nn;
            }
            else
            {
                ListNode<K, T> np = n.Prev, nn = n.Next;
                ListNode<K, T> mp = m.Prev, mn = m.Next;

                if (np != null)
                    np.Next = m;
                m.Prev = np;
                if (nn != null)
                    nn.Prev = m;
                m.Next = nn;

                if (mp != null)
                    mp.Next = n;
                n.Prev = mp;
                if (mn != null)
                    mn.Prev = n;
                n.Next = mn;
            }

           /* if (m == Head && n == Tail)
            {
                ListNode<K, T> tmp = n;
                n = m;
                m = tmp;
            }
            if(n == Head && m == Tail)
            {

                return;
            }


            if (m == Head || m == Tail || m == n.Next)
            {
                ListNode<K, T> tmp = n;
                n = m;
                m = tmp;
            }


            ListNode<K, T> tmpNNext = n.Next;
            ListNode<K, T> tmpNPrev = n.Prev;
            if (n == Head)
            {
                n.Prev = m.Prev;
                n.Prev.Next = n;
                n.Next = m.Next;
                n.Next.Prev = n;
                

                m.Next = tmpNNext;
                m.Next.Prev = m;
                m.Prev = null;
                Head = m;
            }
            else if (n == Tail)
            {
                n.Prev = m.Prev;
                n.Next = m.Next;
                n.Next.Prev = n;
                n.Prev.Next = n;

                m.Prev = tmpNPrev;
                m.Prev.Next = m;
                m.Next = null;
                Tail = m;
            }
            else if (n.Next == m)
            {
                
            }
            else
            {
                n.Prev = m.Prev;
                n.Next = m.Next;
                n.Next.Prev = n;
                n.Prev.Next = n;

                m.Next = tmpNNext;
                m.Prev = tmpNPrev;
                m.Next.Prev = m;
                m.Prev.Next = m;
            }*/
        }
        public void Swap(K key1, K key2)
        {
            ListNode<K, T> cur = Head;
            ListNode<K, T> n = null;
            ListNode<K, T> m = null;
            while (cur != null)
            {
                if (cur.Key.CompareTo(key1) == 0)
                    n = cur;
                if (cur.Key.CompareTo(key2) == 0)
                    m = cur;
                cur = cur.Next;
            }
            Swap(n,m);
        }
    }
}
