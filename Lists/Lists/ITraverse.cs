﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lists
{
    public interface ITraverse<K, T> where K : IComparable<K>
    {
        string Traverse(BTree<K, T> tree);
    }

    public class Preorder<K, T> : ITraverse<K, T> where K : IComparable<K>
    {
        public string Traverse(BTree<K, T> tree)
        {
            StringBuilder s = new StringBuilder();
            RecursivePreorder(tree.Root, s);
            return s.ToString();
        }

        private void RecursivePreorder(TreeNode<K,T> curNode, StringBuilder s)
        {
            s.Append(curNode.ToString()+" | ");
            if(curNode.Left != null)
                RecursivePreorder(curNode.Left,s);
            if(curNode.Right != null)
                RecursivePreorder(curNode.Right,s);
        }
    }
    public class Inorder<K, T> : ITraverse<K, T> where K : IComparable<K>
    {
        public string Traverse(BTree<K, T> tree)
        {
            StringBuilder s = new StringBuilder();
            RecursiveInorder(tree.Root, s);
            return s.ToString();
        }

        private void RecursiveInorder(TreeNode<K, T> curNode, StringBuilder s)
        {
            if (curNode.Left != null)
                RecursiveInorder(curNode.Left, s);
            s.Append(curNode.ToString() + " | ");
            
            if (curNode.Right != null)
                RecursiveInorder(curNode.Right, s);
        }
    }
    public class Postorder<K, T> : ITraverse<K, T> where K : IComparable<K>
    {
        public string Traverse(BTree<K, T> tree)
        {
            StringBuilder s = new StringBuilder();
            RecursivePostorder(tree.Root, s);
            return s.ToString();
        }

        private void RecursivePostorder(TreeNode<K, T> curNode, StringBuilder s)
        {
            
            if (curNode.Left != null)
                RecursivePostorder(curNode.Left, s);
            if (curNode.Right != null)
                RecursivePostorder(curNode.Right, s);
            s.Append(curNode.ToString() + " | ");
        }
    }

    //public class PogoOrder<K, T> : ITraverse<K, T> where K : IComparable<K>
    //{
    //    List<Node<K,T>> done = new List<Node<K, T>>();
    //    public string Traverse(BTree<K, T> tree)
    //    {
    //        StringBuilder s = new StringBuilder();
    //        Random randy = new Random();
    //        while (done.Count < Count(tree))
    //        {
    //            switch (randy.Next(0,4))
    //            {
    //                case 1: GoLeft()
    //            }
    //        }
    //    }

    //    private TreeNode<K, T> GoLeft(TreeNode<K, T> n)
    //    {
    //        return n.Left;
    //    }

    //    private TreeNode<K, T> GoRight(TreeNode<K, T> n)
    //    {
    //        return n.Left;
    //    }
    //    private void Post(TreeNode<K, T> n,StringBuilder s)
    //    {
    //        if (done.Contains(n))
    //        {
    //            done.Add(n);
    //            s.Append(n.Value.ToString());
    //        }
    //    }

    //    private int Count(BTree<K,T> tree)
    //    {
    //        int i = 0;
    //        BTreeIterator<K,T> b = new BTreeIterator<K, T>(tree);
    //        while (b.MoveNext())
    //            i++;
    //        return i;
    //    }

    //}
}
