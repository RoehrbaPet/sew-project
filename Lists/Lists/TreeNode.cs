﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class TreeNode<K, T> : Node<K, T> where K : IComparable<K>
    {
        public TreeNode<K, T> Left { get; set; }
        public TreeNode<K, T> Right { get; set; }

        public TreeNode(K key, T value, TreeNode<K, T> left = null, TreeNode<K, T> right = null)
            : base(key, value)
        {
            this.Left = left;
            this.Right = right;
        }

    }
}
