﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    public class BTreeIterator<K, T> : IEnumerator<TreeNode<K, T>> where K : IComparable<K>
    {
        private BTree<K, T> tree;

        private LinkedList<int, TreeNode<K, T>> nodeStack;
        private int nextIndex = 0;
        private LinkedList<int, bool> printed;

        public TreeNode<K, T> Current { get; private set; }
        object IEnumerator.Current { get { return Current; } }

        public BTreeIterator(BTree<K, T> tree)
        {
            this.tree = tree;

            this.nodeStack = new LinkedList<int, TreeNode<K, T>>();
            this.printed = new LinkedList<int, bool>();
        }

        public void Dispose() //geht das so?
        {
            
        }

        public bool MoveNext()
        {
            if (nodeStack.Head == null)
            {
                if (tree.Root == null)
                    return false;

                Current = tree.Root;
                nodeStack.InsertLast(nextIndex, Current);
                printed.InsertLast(nextIndex++, false);
            }

            if (printed.Tail.Value == false)
            {
                while (Current.Left != null)
                {
                    Current = Current.Left;
                    nodeStack.InsertLast(nextIndex, Current);
                    printed.InsertLast(nextIndex++, false);
                }
                printed.Tail.Value = true;
            }
            else
            {
                if (Current.Right == null)
                {
                    while (nodeStack.Tail != null && printed.Tail.Value == true)
                    {
                        nodeStack.Remove(nodeStack.Tail);
                        printed.Remove(printed.Tail);
                        nextIndex--;
                    }
                    if (nodeStack.Tail != null)
                    {
                        Current = nodeStack.Tail.Value;
                        printed.Tail.Value = true;
                    }
                    return nodeStack.Tail != null;
                }
                else
                {
                    Current = Current.Right;
                    nodeStack.InsertLast(nextIndex, Current);
                    printed.InsertLast(nextIndex++, false);

                    while (Current.Left != null)
                    {
                        Current = Current.Left;
                        nodeStack.InsertLast(nextIndex, Current);
                        printed.InsertLast(nextIndex++, false);
                    }
                    printed.Tail.Value = true;
                }
            }

            return true;
        }

        public void Reset()
        {
            Current = null;
            nodeStack = new LinkedList<int, TreeNode<K, T>>();
            printed = new LinkedList<int, bool>();
        }
    }
}
